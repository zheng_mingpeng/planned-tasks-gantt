## 效果图
![image](https://gitee.com/zheng_mingpeng/planned-tasks-gantt/raw/master/demo.jpg)


## 引用
``` vue
import pageTaskPlan from 'zmp_taskplangantt'
Vue.use(pageTaskPlan)
```

## 单个示例
``` vue
<page-time-plan @bindEmitLineChunk="bindEmitLineChunk" outerWidth="600px" outerHeight="40px"></page-time-plan>
```

## 单个示例包外层
``` vue
<div style="width:700px;height:40px;border:1px solid red">
    <page-time-plan @bindEmitLineChunk="bindEmitLineChunk"></page-time-plan>
</div>
```

## 多个示例
``` vue
<template>
    <div style="width:700px;height:40px;border:1px solid red">
        <page-time-plan v-for="item,index in 10" :key="index"
            :backKey="index"
            :resetSelect="resetSelect"
            @bindEmitLineChunk="bindEmitLineChunk"
        ></page-time-plan>
    </div>
</template>

export default {
    data() {
        return {
            resetSelect: 0,
        }
    },
    methods: {
        // 获取数据
        bindEmitLineChunk(data,key) {
            console.log(data,key)
            this.resetSelect = key
        } 
    }
}
```

## 中间气泡框 可根据自己需求重写，默认用的是element-ui组件
``` vue
那么如果你用的不是element-ui组件你就可以换成自己需要的组件库，如果当前显示不能满足你要求你也可以自己自定义
气泡框内置插槽
<slot :data="item" :index="index" :backKey="backKey"></slot>
```

## 删除字体图标可更改
默认用 element-ui 图标库 可通过deleteIcon配置自己需要的字体图标

## props配置
``` vue
props: {
    readOnly: {  // 只读
        type: Boolean,
        default: false
    },
    outerWidth: {   // 外层宽度
        type: String,
        default: '100%',
    },
    outerHeight: {  // 外层高度
        type: String,
        default: '100%',
    },
    lineNumber: {   // 整个宽度代表多少数值
        type: [Number,String],
        default: 24 * 60 * 60
    },
    initData: {  // 初始化数据
        type: Array,
        default: () =>([])
    },
    formatDataFn: {  // 格式数据函数 左右气泡数据
        type: [Function,String],
        default: ''
    },
    tipPopup: {  // 是否有提示框
        type: Boolean,
        default: true
    },
    boxColor: {  // 移动盒子的颜色
        type: String,
        default: ''
    },
    backParams: { // 需要新增返回参数，用来做一些其他的功能
        type: Object,
        default: () => ({
            startTime: '00:00',
            endTime: '00:00'
        })
    },
    backKey: {  // 返回数据时 带回去的key  主要是多列的时候，让返回数据知道是哪一列返回的
        type: [Number,String],  
        default: 0
    },
    resetSelect: {  // 当有多列时候用来控制时间插件重置的
        type: [Number,String],  
        default: 0
    },
    deleteIcon: {  // 删除图标
        type: String,  
        default: 'el-icon-delete'
    }
}
```

## 自述
上述已经够清楚了吧，要是还是不懂请v50联系作者，待作者吃完肯德基之后，再为你作答。要是发现了bug请不要联系作者，就算联系了我也不会改的。最后附上作者邮箱 52610123@qq.com。

