// 导入单个组件
import pageTaskPlanView from './pageTaskPlan.vue'

// Vue 直接导入
const pageTaskPlan = {
  install: function (Vue) {
    Vue.component('pageTaskPlan', pageTaskPlanView)
  }
}

// 导出
export default pageTaskPlan